/**
 * @file graph_n_clusters.C
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2023-08-11
 *
 * @copyright Copyright (c) 2023
 *
 */

#include <iostream>
#include <vector>
#include <numeric>
#include <algorithm>

#include <TROOT.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TH1D.h>
#include <TF1.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include <TText.h>
#include <TPavesText.h>
#include <TPaveStats.h>
#include <TStyle.h>

#include "modules.h"

TCanvas *get_mean_cluster_size_per_disk_per_ring(TTree *data, const size_t min_x, const size_t max_x, const size_t max_y, const int8_t disk_number, const size_t bar_width, const bool draw, std::string title)
{

    static size_t n = 0;

    const std::array<EColor, 5> colors = {
        kRed,
        kGreen,
        kBlue,
        kOrange,
        kBlack};

    if (bar_width > max_x)
        throw std::invalid_argument("bar_width must not be bigger than max_x");

    if (disk_number > 12 || (disk_number < 9 && disk_number >= 0))
        throw std::invalid_argument("disk_number must be between 9 and 12 or -1 for all disks");

    const int32_t entries = data->GetEntries();

    TCanvas *canvas = new TCanvas();

    if (!draw)
        canvas->SetBatch(true);

    std::string name(title);

    std::replace(name.begin(), name.end(), ' ', '_');

    if (!name.empty())
        name.push_back('_');

    TPad *pad = new TPad((name + "pad_id_" + std::to_string(n)).c_str(), "", 0, 0, 1, 1);

    canvas->SetName((name + "canvas_id_" + std::to_string(n)).c_str());
    canvas->SetTitle(title.c_str());

    std::array<TH1D *, 5> ring_plots;

    size_t k = 1;
    for (auto &ring_plot : ring_plots)
    {
        ring_plot = new TH1D((name + "cluster_size_graph_ring_" + std::to_string(k++) + "_id_" + std::to_string(n)).c_str(), "", (max_x - min_x) / bar_width, min_x, max_x);
    }

    TTreeReader reader(data);

    TTreeReaderValue<std::vector<std::vector<unsigned short>>> clu_size_p(reader, "cluster_nBits");
    TTreeReaderValue<std::vector<std::vector<unsigned char>>> adc_array_p(reader, "adc");
    TTreeReaderValue<std::vector<unsigned char>> disk_p(reader, "disk"), ring_p(reader, "ring");
    TTreeReaderValue<std::vector<unsigned int>> nCluster_array_p(reader, "nClusters");

    constexpr bool use_clu_size = true;

    data->GetEntry(0);


    if (disk_number < 0)
    {
        while (reader.Next())
        {

            for (uint32_t ring_number = 0; ring_number < 5; ring_number++)
            {
                std::vector<uint32_t> ring_indexes;

                std::vector<uint8_t>::iterator it = std::find(ring_p->begin(), ring_p->end(), ring_number + 1);

                while (it != ring_p->end())
                {
                    ring_indexes.push_back(std::distance(ring_p->begin(), it));

                    it = std::find(it + 1, ring_p->end(), (uint8_t)ring_number + 1);
                }


                for (const uint32_t index : ring_indexes)
                {
                    if (use_clu_size)
                    {

                        for (auto nPixels : clu_size_p->at(index))
                        {
                            ring_plots[ring_number]->Fill(nPixels);
                        }
                    }
                    else
                    {
                        ring_plots[ring_number]->Fill(adc_array_p->at(index).size() / nCluster_array_p->at(index));
                    }
                }
            }
        }
    }
    else
    {
        while (reader.Next())
        {
            std::vector<uint32_t> disk_indexes;

            std::vector<uint8_t>::iterator it = std::find(disk_p->begin(), disk_p->end(), disk_number);

            while (it != disk_p->end())
            {
                disk_indexes.push_back(std::distance(disk_p->begin(), it));

                it = std::find(it + 1, disk_p->end(), (uint8_t)disk_number);
            }

            for (uint32_t ring_number = 0; ring_number < 5; ring_number++)
            {

                std::vector<uint32_t> ring_indexes;

                std::copy_if(disk_indexes.begin(), disk_indexes.end(), std::back_inserter(ring_indexes), [&ring_number, &ring_p](const uint32_t val)
                             { return ring_p->at(val) == (ring_number + 1); });

                for (const uint32_t index : ring_indexes)
                {

                    if (use_clu_size)
                    {
                        for (auto nPixels : clu_size_p->at(index))
                        {
                            ring_plots[ring_number]->Fill(nPixels);
                        }
                    }
                    else
                    {
                        ring_plots[ring_number]->Fill(adc_array_p->at(index).size() / nCluster_array_p->at(index));
                    }

                }
            }
        }
    }


    canvas->cd();
    pad->Draw();
    pad->cd();

    gStyle->SetOptFit(111111);

    std::array<TString, 5> peaks;

    for (size_t i = 0; i < 5; i++)
    {
        ring_plots[i]->GetXaxis()->SetTitle(("cluster_size"));
        ring_plots[i]->GetYaxis()->SetTitle("counts");
        ring_plots[i]->SetStats(false);
        ring_plots[i]->SetLineWidth(2);
        ring_plots[i]->SetLineStyle(kDashDotted);
        ring_plots[i]->SetLineColor(colors[i]);
        ring_plots[i]->Draw(i == 0 ? "" : "same");
        ring_plots[i]->GetYaxis()->SetRangeUser(0, max_y);

        double dev = ring_plots[i]->GetStdDev() * 5;
        double peak_x = ring_plots[i]->GetMean();

        TF1 *fit = new TF1("func", "gaus", peak_x - dev, peak_x + dev);

        fit->SetLineStyle(kLine);
        fit->SetLineColor(colors[i]);

        ring_plots[i]->Fit(fit, "QR");

        double peak_poisson = fit->GetParameter(1);

        peaks[i] = Form("R%zu = raw: %0.0f, gaus: %0.0f", i + 1, peak_x, peak_poisson);

        // Add a label near the peak>
        TText *label = new TText(peak_x + 5, fit->Eval(peak_poisson) * 1.05 + 15, ("R" + std::to_string(i + 1)).c_str());
        label->SetName((title + "cluster_size_label_ring_" + std::to_string(i + 1) + "_id_" + std::to_string(n)).c_str());
        label->SetTextSize(0.03);
        label->SetTextAngle(0);
        label->SetTextColor(kBlack);
        label->SetTextAlign(22); // Center alignment both horizontally and vertically
        label->Draw();
    }

    TPaveText *title_text = new TPaveText(0.1, 0.95, 0.9, 1.0, "brNDC");
    title_text->SetName(("cluster_size_title_text_id_" + std::to_string(n)).c_str());
    title_text->SetBorderSize(0);
    title_text->AddText(title.c_str());
    title_text->SetFillColor(0);   // Fill color (0 is transparent)
    title_text->SetTextFont(42);   // Font style
    title_text->SetTextSize(0.05); // Font size
    title_text->SetTextAlign(22);  // Center alignment
    title_text->Draw();

    // Create an empty TPaveStats box
    TPaveStats *stats = new TPaveStats(0.6, 0.6, 0.9, 0.9, "brNDC"); // Coordinates are given in the fraction of the pad size
    stats->SetName("cluster_size_per_ring");
    stats->AddText(title.c_str());
    stats->SetFillColor(0);
    stats->SetBorderSize(1);
    stats->SetTextSize(0.03);
    stats->Draw();

    for (size_t i = 0; i < colors.size(); i++)
    {
        stats->AddText(peaks[i]);
    }

    n++;

    return canvas;
}