/**
 * @file modules.h
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2023-08-25
 *
 * @copyright Copyright (c) 2023
 *
 */

#ifndef IT_ANALYSIS_H
#define IT_ANALYSIS_H

#include <tuple>
#include <vector>
#include <TROOT.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TH2.h>

/**
 * @brief Get the pileup values
 *
 * @param data tuple from get_tree_data
 * @return std::vector<double> pileup values
 */
std::vector<double> get_pu(std::tuple<std::vector<double>, std::vector<TTree *>, std::vector<TFile *>> data)
{
    return get<0>(data);
}

/**
 * @brief Get the trees
 *
 * @param data  tuple from get_tree_data
 * @return std::vector<TTree *> tree pointers
 */
std::vector<TTree *> get_trees(std::tuple<std::vector<double>, std::vector<TTree *>, std::vector<TFile *>> data)
{
    return get<1>(data);
}

/**
 * @brief Get the tree data from a specifc directory
 *
 * @param dir directory to retrieve the data from
 * @brief retrieves tree files from a specific directory, the filename must contain information about the pileup in the following way: {FILENAME}_PU_{PILEUP}_.root
 * @return std::vector<double> pileup values, std::vector<TTree*> tree pointers, std::vector<TFile*> file pointers
 */
std::tuple<std::vector<double>, std::vector<TTree *>, std::vector<TFile *>> get_tree_data(std::string dir);

/**
 * @brief generates 2 2d and 1d histograms of the size maximum width and height. 2d histogram shows per entry and 1d shows total
 *
 * @param data tree file to retrieve the information from
 * @param max_dimensions max width and height visualised on the 2d histogram
 * @param title title of the graph
 * @return 2d histogram for horizontal axis and vertical axis
 */
std::tuple<TCanvas *, TCanvas *> get_cluster_dimensions(TTree *data, const uint32_t max_dimensions, const std::string title = "cluster dimensions");

/**
 * @brief create a histogram of the distribution of clustersizes
 *
 * @param data tree file to retrieve the information from
 * @param max_x max size to be visualised on the histogram
 * @param bin_width how much must the data be grouped up, is nice for when data is very spaced out
 * @param title title of graph
 * @return pointer to histogram
 */
TH1D *get_cluster_size(TTree *data, const size_t max_x, const uint32_t bin_width = 10, const std::string title = "cluster size");

/**
 * @brief Create a hitmap showing clusters for a specific module
 *
 * @param data Pointer to the TTree containing the data
 * @param side_number Identification number for the side of the detector
 * @param disk_number Identification number for the disk
 * @param ring_number Identification number for the ring
 * @param layer_number Identification number for the layer
 * @param module_number Identification number for the module
 * @param entry Entry index for the specific event
 * @param draw Boolean indicating if the hitmap should be drawn
 * @param title Optional title for the plot
 * @return A tuple containing a TH2D representing the hitmap hitmap and a vector of TBox pointers showing clusters
 */
std::tuple<TH2D *, std::vector<TBox *>> get_module_hitmap_with_clusters(TTree *data, const uint32_t side_number, const uint32_t disk_number, const uint32_t ring_number, const uint32_t layer_number, const uint32_t module_number, const ssize_t entry = -1, const bool draw = true, const std::string title = "hitmap");

/**
 * @brief Generate a hitmap for a specific module
 *
 * @param data Pointer to the TTree containing the data
 * @param side_number Identification number for the side of the detector
 * @param disk_number Identification number for the disk
 * @param ring_number Identification number for the ring
 * @param layer_number Identification number for the layer
 * @param module_number Identification number for the module
 * @param entry Entry index for the specific event
 * @param draw Boolean indicating if the hitmap should be drawn
 * @param title Optional title for the plot
 * @return Pointer to a TH2D representing the hitmap
 */
TH2D *get_module_hitmap(TTree *data, const uint32_t side_number, const uint32_t disk_number, const uint32_t ring_number, const uint32_t layer_number, const uint32_t module_number, const ssize_t entry = -1, const bool draw = true, const std::string title = "hitmap");

/**
 * @brief Generate and plot the number of clusters per module for a specific disk
 *
 * @param data Pointer to the TTree containing the data
 * @param max_x Maximum x-axis value for the histogram
 * @param disk_number Identification number for the disk
 * @param bin_width Width of each bar in the histogram
 * @param title Optional title for the plot
 * @return Pointer to TH1D representing the histogram of #clusters per module
 */
TH1D *get_nClusters_per_disk_per_module(TTree *data, const size_t max_x, const int8_t disk_number, const uint32_t bin_width = 10, const std::string title = "nClusters per module");

/**
 * @brief Generate and plot the number of clusters per module, per ring, for a specific disk
 *
 * @param data Pointer to the TTree containing the data
 * @param max_x Maximum x-axis value for the histogram
 * @param max_y Maximum y-axis value for the histogram
 * @param disk_number Identification number for the disk
 * @param bin_width Width of each bar in the histogram
 * @param title Optional title for the plot
 * @return Pointer to a TCanvas for displaying the histogram
 */
TCanvas *get_nClusters_per_disk_per_ring_per_module(TTree *data, const size_t max_x, const size_t max_y, const int8_t disk_number, const uint32_t bin_width = 10, const std::string title = "nClusters per ring");

/**
 * @brief Generate and plot the number of hits per module for a specific disk
 *
 * @param data Pointer to the TTree containing the data
 * @param max_x Maximum x-axis value for the histogram
 * @param disk_number Identification number for the disk
 * @param bin_width Width of each bar in the histogram
 * @param title Optional title for the plot
 * @return Pointer to TH1D representing the histogram of hits per module
 */
TH1D *get_nHits_per_module(TTree *data, const size_t max_x, const int8_t disk_number, const uint32_t bin_width = 10, const std::string title = "nHits per module");

/**
 * @brief Generate and plot the peak number of clusters as a function of pile-up
 *
 * @param nCluster_graphs Vector of TH1D pointers representing various nCluster histograms
 * @param pile_up Vector of doubles representing the pile-up values
 * @param fit_proportional Boolean indicating if the fit should be proportional
 * @param draw Boolean indicating if the plot should be drawn
 * @param title Optional title for the plot
 * @return Pointer to a TCanvas for displaying the plot
 */
TCanvas *get_peak_per_pileup_graph(const std::vector<TH1D *> &nCluster_graphs, const std::vector<double> &pile_up, const bool fit_proportional = false, const bool draw = true, const std::string title = "Mean cluster per pileup; <PU>; Mean cluster");

/**
 * @brief Generate and plot a graph of values as a function of pile-up
 *
 * @param values Vector of doubles representing the values
 * @param pile_up Vector of doubles representing the pile-up values
 * @param min_fit Minimum fitting value
 * @param max_fit Maximum fitting value
 * @param title Optional title for the plot
 * @return Pointer to a TCanvas for displaying the plot
 */
TCanvas *get_value_per_pileup_graph(const std::vector<double> &values, const std::vector<double> &pile_up, const double min_fit, const double max_fit, const std::string title = "value per pileup; pileup; value");

/**
 * @brief Generate and plot a graph of values per ring as a function of pile-up
 *
 * @param values An array containing vectors of values for each ring
 * @param pile_up Vector of doubles representing the pile-up values
 * @param min_fit Minimum fitting value
 * @param max_fit Maximum fitting value
 * @param title Optional title for the plot
 * @return Pointer to a TCanvas for displaying the plot
 */
TCanvas *get_value_per_ring_per_pileup(const std::array<std::vector<double>, 5> &values, const std::array<std::vector<double>, 5> errors, const std::vector<double> &pile_up, const double min_fit, const double max_fit, const std::string title);

/**
 * @brief Get faults in clustering by comparing with reference data
 *
 * @param data Pointer to the TTree containing the data
 * @param reference Pointer to the TTree containing the reference data
 * @return A tuple containing vectors of doubles representing the faults, one for position and one for size and position
 */
std::tuple<std::vector<double>, std::vector<double>> get_faults_clustering(TTree *data, TTree *reference);

/**
 * @brief Get the ratio per ring per pileup
 *
 * @param data data to calculate the ratio on
 * @param reference reference data
 * @param pile_up pileup per entry
 * @return TCanvas*
 */
TCanvas *get_ratio_per_ring_per_pileup(const std::array<std::vector<double>, 5> &data, const std::array<std::vector<double>, 5> &reference, const std::vector<double> &pile_up, const bool in_percentage = false, const bool draw = true, const std::string title = "ratio", const std::string y_axis_title = "ratio");

/**
 * @brief Generates a TCanvas object that displays the mean ratio of clusters between 2 datasets per ring as a function of pile-up.
 *
 * The function takes vectors of TTree pointers for both the data and reference sets. It then uses the pile-up information
 * and disk number to create a TCanvas that represents the mean ratio per ring per pile-up.
 *
 * @param data Vector of TTree pointers containing the data set.
 * @param reference Vector of TTree pointers containing the reference set.
 * @param pile_up Vector of double values indicating the pile-up for each corresponding TTree in the data and reference sets.
 * @param disk_number Integer value specifying the disk number to consider in the analysis.
 * @param title Optional string to set as the title for the TCanvas. Default is an empty string.
 *
 * @return TCanvas* Pointer to the generated TCanvas object.
 */
TCanvas *get_mean_ratio_per_ring_per_pileup(std::vector<TTree *> &data, std::vector<TTree *> &reference, const std::vector<double> &pile_up, const int16_t min_cluster_size, const int8_t disk_number, const std::string title = "mean ratio per ring per pileup");

/**
 * @brief Get the nCluster per disk per ring object
 *
 * @param data data to calculate the ratio on
 * @param min_x Minimum x-axis value for the histogram
 * @param max_x Maximum x-axis value for the histogram
 * @param max_y Maximum y-axis value for the histogram
 * @param min_cluster_size minimum cluster size of the cluster
 * @param disk_number disk number
 * @param bin_width bin width
 * @param draw if canvas must be drawn
 * @param title title of the graph
 * @return TCanvas*
 */
TCanvas *get_nClusters_per_disk_per_ring(TTree *data, const size_t min_x, const size_t max_x, const size_t max_y, const int16_t min_cluster_size, const int8_t disk_number, const size_t bin_width = 100, const bool draw = true, std::string title = "mean clusters per ring");

/**
 * @brief Get the nCluster per disk per ring per pileup object
 *
 * @param data data to perform the calculations on
 * @param pile_up Vector of doubles representing the pile-up values
 * @param min_cluster_size minimum cluster size of the cluster
 * @param min_fit Minimum fitting value
 * @param max_fit Maximum fitting value
 * @param disk_number disk number
 * @param title title of the graph
 * @return TCanvas*
 */
TCanvas *get_nClusters_per_disk_per_ring_per_pileup(std::vector<TTree *> &data, const std::vector<double> &pileup, const int16_t min_cluster_size, const double min_fit, const double max_fit, const int8_t disk_number, const std::string title = "nCluster per disk per ring per pileup");

/**
 * @brief Get the fraction of clusters bigger than one per ring per pileup
 *
 * @param data data to perform the calculations on
 * @param pile_up Vector of doubles representing the pile-up values
 * @param disk_number disk number
 * @param title title of the graph
 * @return TCanvas*
 */
TCanvas *get_fraction_of_clusters_bigger_than_one_per_ring_per_pileup(std::vector<TTree *> &data, const std::vector<double> &pile_up, const int8_t disk_number, const std::string title);

/**
 * @brief Get the mean cluster size per disk per ring object
 *
 * @param data data to perform the calculations on
 * @param min_x Minimum x-axis value for the histogram
 * @param max_x Maximum x-axis value for the histogram
 * @param max_y Maximum y-axis value for the histogram
 * @param disk_number disk number
 * @param bin_width bin width
 * @param draw draw on screen
 * @param title title of the graph
 * @return TCanvas*
 */
TCanvas *get_mean_cluster_size_per_disk_per_ring(TTree *data, const size_t min_x, const size_t max_x, const size_t max_y, const int8_t disk_number, const size_t bin_width, const bool draw, std::string title = "mean cluster size per disk per ring");

/**
 * @brief Get the mean cluster size per disk per ring per pileup object
 *
 * @param data data to perform the calculations on
 * @param pileup pileup per data entry
 * @param min_fit minimum fit range
 * @param max_fit maximum fit range
 * @param disk_number disk number
 * @param title title of the graph
 * @return TCanvas*
 */
TCanvas *get_mean_cluster_size_per_disk_per_ring_per_pileup(std::vector<TTree *> &data, const std::vector<double> &pileup, const double min_fit, const double max_fit, const int8_t disk_number, const std::string title = "mean cluster size per disk per ring per pileup");

/**
 * @brief export a set of variables to a .csv file
 * 
 * @param file_name name of the file
 * @param variable_names names of the variables
 * @param data dataset
 */
void export_vars_to_csv(std::string file_name, std::vector<std::string> variable_names, std::vector<std::vector<double>> data);

#endif