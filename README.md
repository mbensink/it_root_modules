# IT Analysis

## Overview

This repository contains a collection of C++ functions for data analysis of the Inner Tracker Endcap detectors (IT TEPX). It leverages ROOT's `TTree`, `TFile`, and various other types for creating and displaying histograms and canvases related to cluster dimensions, sizes, and hitmaps.

## Dependencies

* ROOT
* C++ Standard Library

## Getting Started

To get started, include `modules.h` in your C++ project. Make sure to install all required dependencies.

### Compilation

To compile, you'll need to link against ROOT's libraries.

### Running on LXPLUS

```bash
CMSSW_VERSION="CMSSW_13_0_10"
cmsrel "${CMSSW_VERSION}"
cd "${CMSSW_VERSION}/src"
cmsenv
git clone https://gitlab.cern.ch/mbensink/it_root_modules.git

```

### Module Loading in ROOT CLI

You can automatically load the available C++ modules into the ROOT CLI environment using the `load_modules` function. To do this, execute ROOT with `load_modules.C` as an argument. This will automatically scan the current directory for files with the `.C` extension and load them into the ROOT environment.

**How To Use `load_modules()`**

1. Make sure you are in the directory containing all your `.C` files.
2. Start ROOT with the `load_modules.C` script as an argument:

   ```bash
   root load_modules.C
   ```

 **Important** : The function is designed to be called only once per ROOT session. If you try to execute it more than once, a runtime error will be thrown.

## Documentation

### Header File: modules.h

#### Functions

1. **get_tree_data**
   Retrieves tree files from a specific directory. The filename must contain information about the pile-up.

   ```cpp
   std::tuple<std::vector<double>, std::vector<TTree*>, std::vector<TFile*>> get_tree_data(std::string dir);
   ```
2. **get_cluster_dimensions**
   Generates histograms showing the cluster dimensions.

   ```cpp
   std::tuple<TCanvas *, TCanvas *> get_cluster_dimensions(TTree *data, const uint32_t max_dimensions, const std::string title = "cluster dimensions");
   ```

   **get_cluster_size**
3. Creates a histogram showing the distribution of cluster sizes.

   ```cpp
   TH1D *get_cluster_size(TTree *data, const size_t max_x, const uint32_t bar_width = 10, const std::string title = "cluster size");
   ```

   **get_module_hitmap_with_clusters**
4. Creates a hitmap showing clusters for a specific module.

   ```cpp
   std::tuple<TH2D *, std::vector<TBox *>> get_module_hitmap_with_clusters(TTree *data, const uint32_t side_number, ...);
   ```

   **get_module_hitmap**
5. Generates a hitmap for a specific module.

   ```cpp
   TH2D * get_module_hitmap(TTree *data, const uint32_t side_number, ...);
   ```

   **get_nClusters_per_disk_per_module**
6. Generates and plots the number of clusters per module for a specific disk.

   ```cpp
   TH1D *get_nClusters_per_disk_per_module(TTree *data, const size_t max_x, const int8_t disk_number, ...);
   ```

   **get_nClusters_per_disk_per_ring_per_module**
7. Generates and plots the number of clusters per module, per ring, for a specific disk.

   ```cpp
   TCanvas *get_nClusters_per_disk_per_ring_per_module(TTree *data, const size_t max_x, const size_t max_y, ...);
   ```
8. **get_nClusters_per_disk_per_ring**
   Generates and plots the number of clusters per ring for a specific disk.

   ```cpp
   TCanvas *get_nClusters_per_disk_per_ring(TTree *data, const size_t min_x, const size_t max_x, ...);
   ```
9. **get_nHits_per_module**
   Generates and plots the number of hits per module for a specific disk.

   ```cpp
   TH1D *get_nHits_per_module(TTree *data, const size_t max_x, ...);
   ```
10. **get_peak_per_pileup_graph**
    Generates and plots the peak number of clusters as a function of pile-up.

    ```cpp
    TCanvas *get_peak_per_pileup_graph(const std::vector<TH1D *> &nCluster_graphs, const std::vector<double> &pile_up, ...);
    ```
11. **get_value_per_pileup_graph**
    Generates and plots a graph of values as a function of pile-up.

    ```cpp
    TCanvas *get_value_per_pileup_graph(const std::vector<double> &values, ...);
    ```
12. **get_value_per_ring_per_pileup**
    Generates and plots a graph of values per ring as a function of pile-up.

    ```cpp
    TCanvas *get_value_per_ring_per_pileup(const std::array<std::vector<double>, 5> &values, ...);
    ```
13. **get_mean_cluster_per_ring_per_pileup**
    Generates and plots the mean number of clusters per ring as a function of pile-up.

    ```cpp
    TCanvas * get_mean_cluster_per_ring_per_pileup(std::vector<TTree *> &trees, ...);
    ```
14. **get_faults_clustering**
    Gets faults in clustering by comparing with reference data.

    ```cpp
    std::tuple<std::vector<double>, std::vector<double>> get_faults_clustering(TTree *data, TTree *reference);
    ```
15. **get_ratio_per_ring_per_pileup**
    Gets the ratio per ring per pileup.

    ```cpp
    TCanvas *get_ratio_per_ring_per_pileup(const std::array<std::vector<double>, 5> &data, ...);
    ```
16. **get_mean_ratio_per_ring_per_pileup**
    Generates a TCanvas that displays the mean ratio of clusters between 2 datasets per ring as a function of pile-up.

    ```cpp
    TCanvas *get_mean_ratio_per_ring_per_pileup(std::vector<TTree *> &data, std::vector<TTree *> &reference, ...);
    ```

## Usage Example

Setup root CLI:

```bash
root load_modules.C
```

Load data:

```cpp
auto data = get_tree_data("your root file directory");
// extract pileups and tree files
auto pileups = get_pu(data);
auto trees = get_trees(data);
```

Plot nClusters and nClusters per ring per module:

```cpp
get_nClusters_per_disk_per_module(trees[0], 100, 9, 1, "nClusters for disk 9 per module PU" + std::to_string(pu[0]))->Draw();
get_nClusters_per_disk_per_ring_per_module(trees[0], 100, 15000, 9, 1, "nClusters for disk 9 per module PU" + std::to_string(pu[0]))->Draw();
```

plot hitmap of a module

```cpp
get_module_hitmap_with_clusters(trees[0], 1, 9, 1, 1, 1, 1, true, "hitmap S1D9R1L1M1");
```

more usage examples can be found in the example folder.

#### **images**

![fraction](https://gitlab.cern.ch/mbensink/it_root_modules/-/raw/master/examples/images/fraction_of_clusters_bigger_than_one.png)

![hitmap](https://gitlab.cern.ch/mbensink/it_root_modules/-/blob/master/examples/images/hitmap.png)

![mean_cluster_size](https://gitlab.cern.ch/mbensink/it_root_modules/-/raw/master/examples/images/mean_cluster_size.png)

![per_ring_per_pileup](https://gitlab.cern.ch/mbensink/it_root_modules/-/raw/master/examples/images/per_ring_per_pileup.png)

![mean_clusters_per_ring](https://gitlab.cern.ch/mbensink/it_root_modules/-/raw/master/examples/images/mean_clusters_per_ring_PU_120.png)

## Contributing

Feel free to submit pull requests to enhance the functionalities of this library.
