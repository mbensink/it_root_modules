/**
 * @file get_mean_per_pileup_graph.C
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2023-08-22
 *
 * @copyright Copyright (c) 2023
 *
 */

#include <vector>
#include <cinttypes>
#include <numeric>
#include <algorithm>

#include <TH1D.h>
#include <TGraph.h>
#include <TCanvas.h>
#include <TGraphErrors.h>
#include <TLatex.h>
#include <TPaveLabel.h>
#include <TStyle.h>
#include <TF1.h>

// #include "modules.h"

TCanvas *get_peak_per_pileup_graph(const std::vector<TH1D *> &nCluster_graphs, const std::vector<double> &pile_up, const bool fit_proportional, const bool draw, const char *title)
{
    static size_t n  = 0;

    if (pile_up.size() != nCluster_graphs.size())
        throw std::invalid_argument("pile up and ncluster_graphs should have the same size");

    if (pile_up.empty() || nCluster_graphs.empty())
        throw std::invalid_argument("either pile up or ncluster_graphs doesnt have any values");

    const auto [pile_up_min, pile_up_max] = std::minmax_element(pile_up.begin(), pile_up.end());

    std::vector<double> mean_values;
    
    for (TH1D *graph : nCluster_graphs)
    {
        mean_values.push_back(graph->GetMaximum());
    }

    std::vector<std::pair<double, int>> aux(pile_up.size());

    for (int i = 0; i < pile_up.size(); i++)
    {
        aux[i] = {pile_up[i], i}; // element, original index
    }

    sort(aux.begin(), aux.end(), [](const std::pair<double, int> &a, const std::pair<double, int> &b)
         { return a.first < b.first; });

    std::vector<double> x,y, error;

    for (const auto i : aux)
    {
        x.push_back(i.first);
        y.push_back(mean_values[i.second]);
    }

    TCanvas *canvas = new TCanvas();

    TPad *plot_pad = new TPad("pad1", "pad 1", 0, 0.3, 1, 1);
    TPad *plot_error_pad = new TPad("pad2", "pad 2", 0, 0, 1, 0.3);

    plot_pad->Draw();
    plot_error_pad->Draw();

    TGraph *plot = new TGraph(mean_values.size(), x.data(), y.data());
    plot->SetName((std::string("MEAN_PER_PILEUP") + std::to_string(n++)).c_str());

    TF1 *fit = new TF1("f1", "[0] + [1]*x", *pile_up_min, *pile_up_max);

    plot_pad->cd();

    plot->Fit(fit);

    plot->SetTitle(title);

    plot->SetLineWidth(2);

    plot->Draw("AC*");

    double a = fit->GetParameter(1);
    double b = fit->GetParameter(0);
    TLatex latex;
    latex.SetNDC();
    latex.SetTextSize(0.04);
    TString fitStr = Form("y = %.3f x + %.3f", a, b);
    latex.DrawLatex(0.6, 0.85, fitStr); 

    plot_error_pad->cd();

    for(int i = 0; i < y.size(); i++)
    {
        const auto y_fit = fit->Eval(x[i]);

    
        error.push_back(fit_proportional ? ((y_fit - y[i]) / y_fit) * 100.0 : y_fit - y[i]);
    }
    
    TGraph *error_plot = new TGraph(mean_values.size(), x.data(), error.data());
    error_plot->SetName((std::string("MEAN_PER_PILEUP_ERROR") + std::to_string(n++)).c_str());
    error_plot->SetTitle(fit_proportional ? "; <PU>; proportional error (%)" : "; <PU>; error");

    error_plot->GetXaxis()->SetTitleOffset(0.3);
    error_plot->GetXaxis()->SetTitleSize(0.1);
    error_plot->GetXaxis()->SetLabelSize(0.08);
    error_plot->GetYaxis()->SetTitleOffset(0.3);
    error_plot->GetYaxis()->SetTitleSize(0.1);
    error_plot->GetYaxis()->SetLabelSize(0.08);

    error_plot->Draw("AC *");
    return canvas;
}