/**
 * @file get_mean_cluster_per_ring_per_pileup.C
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2023-08-25
 *
 * @copyright Copyright (c) 2023
 *
 */

#include <vector>
#include <cinttypes>
#include <numeric>
#include <algorithm>
#include <string>
#include <fstream>

#include <TGraph.h>
#include <TCanvas.h>
#include <TPad.h>
#include <TGraphErrors.h>

#include <TF1.h>

#include "modules.h"

TCanvas *get_nClusters_per_disk_per_ring_per_pileup(std::vector<TTree *> &data, const std::vector<double> &pileup, const int16_t nPixles_min_size, const double min_fit, const double max_fit, const int8_t disk_number, const std::string title)
{
    const std::string identifier("mean_non_single_graph_ring_");

    if (data.size() != pileup.size())
        throw std::invalid_argument("length of data and pileup vector must be the same");

    if (data.empty() || pileup.empty())
        throw std::invalid_argument("either pileup vector or threes vector is empty");

    std::string name(title);

    std::replace(name.begin(), name.end(), ' ', '_');

    std::array<std::vector<double>, 5> mean_graph, errors;

    for (size_t index = 0; index < data.size(); index++)
    {

        TCanvas *c = get_nClusters_per_disk_per_ring(data[index], 0, 10e5, 5000, nPixles_min_size ,disk_number, 50, false, "");

        TVirtualPad *vpad = c->GetPad(0);

        TPad *pad = (TPad *)TIter(vpad->GetListOfPrimitives())();

        TList *items = pad->GetListOfPrimitives();

        TIter it(items);
        TObject *obj;

        while ((obj = it()))
        {
            std::string obj_name(obj->GetName());

            size_t pos = obj_name.find(identifier);

            if (pos == std::string::npos)
                continue;

            size_t ring = obj_name[pos + identifier.size()] - 48;

            TH1D *graph = static_cast<TH1D *>(obj);

            TF1 *func = graph->GetFunction("func");

            if (func == nullptr)
                throw std::runtime_error("could not find fit of function");

            mean_graph[ring - 1].push_back(graph->GetMean());
            errors[ring - 1].push_back(graph->GetRMS()/std::sqrt(graph->GetEntries()));
        }

        delete c;
    }

    return get_value_per_ring_per_pileup(mean_graph, errors , pileup, min_fit, max_fit, title + " mean per ring per pileup [min clustersize = " + std::to_string(nPixles_min_size) + "]; pileup; mean cluster total per ring");
}
